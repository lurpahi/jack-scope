extern crate jack;
extern crate sdl2;

use std::sync::mpsc::channel;
use std::time::Duration;
use std::cmp::min;
use std::thread;

use sdl2::event::Event;
use sdl2::keyboard::Keycode;
use sdl2::pixels::Color;
use sdl2::rect::Point;

fn main() {
  // JACK init
  let (client, _status) =
    jack::Client::new("jack-scope", jack::ClientOptions::NO_START_SERVER).unwrap();
  let l_in_port = client.register_port("in_x", jack::AudioIn::default()) .unwrap();
  let r_in_port = client.register_port("in_y", jack::AudioIn::default()) .unwrap();
  // SDL2 init
  let sdl_context = sdl2::init().unwrap();
  let vid_subsys  = sdl_context.video().unwrap();
  let window      = vid_subsys.window("", 1920, 1080)
    .position_centered().resizable().build().unwrap();
  let mut canvas  = window.into_canvas().present_vsync().build().unwrap();
  canvas.clear(); canvas.present();
  // etc
  let (tx, rx) = channel();
  let mut x_y_mode = false;
  let     x_y_len: usize = (client.sample_rate() / 60) * 2;
  let mut linear_len:     usize = client.sample_rate() / (canvas.output_size().unwrap().1 / 60) as usize;
  let mut linear_buffer:    Vec<f32> = Vec::new();
  let mut x_y_buffer: Vec<(f32,f32)> = Vec::new();

  // JACK stuff  
  let process = jack::ClosureProcessHandler::new(
    move |_: &jack::Client, ps: &jack::ProcessScope| -> jack::Control {
      let lin = l_in_port.as_slice(ps);
      let rin = r_in_port.as_slice(ps);
      for i in 0..lin.len() {
        tx.send((lin[i],rin[i])).unwrap();
      }
      jack::Control::Continue
    },
  );

  let active_client = client.activate_async((), process).unwrap();
  
  'mainloop: loop {
    for event in sdl_context.event_pump().unwrap().poll_iter() {/*{{{*/
      match event {
        Event::Quit { .. } |
        Event::KeyDown { keycode: Some(Keycode::Q), .. } => break 'mainloop,
        Event::KeyDown { keycode: Some(keycode), .. } => match keycode {
          Keycode::X => { 
            if x_y_mode {
              x_y_mode = false;
              x_y_buffer.clear();
            } else {
              x_y_mode = true;
              linear_buffer.clear();
            }
          },
          Keycode::Left  | Keycode::Minus  => if linear_len > 100 { linear_len -= 100 },
          Keycode::Right | Keycode::Equals => linear_len += 100,
          _ => (),
        }
        _ => (),
      }
    }/*}}}*/
    let (size_x, size_y) = canvas.output_size().unwrap();
    let (half_x, half_y) = (size_x as f32 / 2.0, size_y as f32 / 2.0);
    if x_y_mode {
      let buffer_len = x_y_buffer.len();
      if buffer_len < x_y_len {
        for _ in 0..(x_y_len - x_y_buffer.len()) {
          x_y_buffer.push((half_x, half_y));
        }
      } else if buffer_len > x_y_len {
        x_y_buffer.truncate(x_y_len);
      }
    } else {
      let buffer_len = linear_buffer.len();
      if buffer_len < linear_len {
        for _ in 0..(linear_len - linear_buffer.len()) {
          linear_buffer.push(half_y);
        }
      } else if buffer_len > linear_len {
        linear_buffer.truncate(linear_len);
      }
    }
    
    while let Ok((l,r)) = rx.try_recv() {
      if x_y_mode {
        x_y_buffer.remove(0);
        x_y_buffer.push((l * half_y + half_x, size_y as f32 - (r * half_y + half_y)));
      } else {
        let n = (l+r) / 2.0;
        linear_buffer.remove(0);
        linear_buffer.push(n * half_y + half_y);
      }
      if l.abs() >= 1.0 { println!("CLIPPING"); }
      if r.abs() >= 1.0 { println!("CLIPPING"); }
    }
    
    if x_y_mode {
      for i in 1..x_y_buffer.len()-2 {
        let (x0,y0) = x_y_buffer[i-1];
        let (x1,y1) = x_y_buffer[i];
        let (x2,y2) = x_y_buffer[i+1];
        let (x3,y3) = x_y_buffer[i+2];
        let c = min(std::u8::MAX as usize, i / (x_y_len / std::u8::MAX as usize)) as u8;
        canvas.set_draw_color(Color::RGB(c,c,c));
        catmull_rom(&mut canvas,
                    ((x1 - x2).powi(2) + (y1 - y2).powi(2)).sqrt() as u32 / 4 + 1,
          [Point::new(x0 as i32, y0 as i32), Point::new(x1 as i32, y1 as i32),
           Point::new(x2 as i32, y2 as i32), Point::new(x3 as i32, y3 as i32)]).unwrap();
        //canvas.draw_line(
        //  Point::new(x1 as i32, y1 as i32),
        //  Point::new(x2 as i32, y2 as i32)
        //).unwrap();
      }
    } else {
      canvas.set_draw_color(Color::RGB(255,255,255));
      let ratio = size_x as f32 / linear_len as f32;
      for i in 1..linear_buffer.len() {
        let point_x = size_x - (i as f32 * ratio) as u32;
        canvas.draw_line(
          Point::new(point_x as i32, linear_buffer[i-1] as i32),
          Point::new((point_x as f32 - ratio) as i32, linear_buffer[i] as i32)
        ).unwrap();
      }
    }
    canvas.present();
    canvas.set_draw_color(Color::RGB(0,0,0));
    canvas.clear();
    thread::sleep(Duration::new(0, 1_000_000_000u32 / 60));
  }
  active_client.deactivate().unwrap();
}

fn catmull_rom(canvas: &mut sdl2::render::Canvas<sdl2::video::Window>,
               segments: u32,
               points: [Point; 4]) -> Result<(), String> {
  fn q(t: f32, points: [Point; 4]) -> Point {
    let alpha = 0.5; // centripetal, I guess
    let ps: Vec<(f32,f32)> = points.iter().map(|p| (p.x as f32, p.y as f32)).collect();
      
    Point::new((alpha * ((2.0 * ps[1].0) +
                         (-ps[0].0 + ps[2].0) * t +
                         (2.0 * ps[0].0 - 5.0 * ps[1].0 + 4.0 * ps[2].0 - ps[3].0) * t.powi(2) +
                         (-ps[0].0 + 3.0 * ps[1].0 - 3.0 * ps[2].0 + ps[3].0) * t.powi(3))) as i32,
               (alpha * ((2.0 * ps[1].1) +
                         (-ps[0].1 + ps[2].1) * t +
                         (2.0 * ps[0].1 - 5.0 * ps[1].1 + 4.0 * ps[2].1 - ps[3].1) * t.powi(2) +
                         (-ps[0].1 + 3.0 * ps[1].1 - 3.0 * ps[2].1 + ps[3].1) * t.powi(3))) as i32)
  } 
  let mut out: Vec<Point> = Vec::new();
  for t in 0..=segments {
    let t = t as f32 / segments as f32;
    out.push(q(t,points));
  }
  for i in 1..out.len() {
    canvas.draw_line(out[i], out[i-1])?;
  }
  Ok(())
}

